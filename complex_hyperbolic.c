// This file has been created on March-02-2020
// Copyright (c) 2020 Abanoub Sameh

#include <math.h>

#include "headers/Complex.h"
#include "headers/complex_algebra.h"
#include "headers/complex_power.h"
#include "headers/complex_hyperbolic.h"

// hyperbolic functions
Complex sinh_complex(Complex angle) {
    Complex num = new_Complex_algeb(sinh(angle.real) * cos(angle.imag),
                        sin(angle.imag) * cosh(angle.real));
    return num;
}

Complex sinh_real(double angle) {
    Complex num = new_Complex_algeb(sinh(angle), 0);
    return num;
}

Complex sinh_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = sinh_complex(temp);
    return num;
}

Complex cosh_complex(Complex angle) {
    Complex num = new_Complex_algeb(cosh(angle.real) * cos(angle.imag),
                        sinh(angle.real) * sin(angle.imag));
    return num;
}

Complex cosh_real(double angle) {
    Complex num = new_Complex_algeb(cosh(angle), 0);
    return num;
}

Complex cosh_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = cosh_complex(temp);
    return num;
}

Complex tanh_complex(Complex angle) {
    Complex nom = sinh_complex(angle);
    Complex denom = cosh_complex(angle);
    Complex num = div_complex_numbers(nom, denom);
    return num;
}

Complex tanh_real(double angle) {
    Complex num = new_Complex_algeb(tanh(angle), 0);
    return num;
}

Complex tanh_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = tanh_complex(temp);
    return num;
}

// inverse hyperbolic functions
Complex asinh_complex(Complex angle) {
    Complex pow = complex_pow_real(angle, 2);
    Complex root = add_complex_by_real(pow, 1);
    Complex ans = complex_pow_real(root, 0.5);
    Complex add = add_complex_numbers(ans, angle);
    Complex num = complex_log_real(add, M_E);
    return num;
}

Complex asinh_real(double angle) {
    Complex num = new_Complex_algeb(asinh(angle), 0);
    return num;
}

Complex asinh_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = asinh_complex(temp);
    return num;
}

Complex acosh_complex(Complex angle) {
    Complex root1 = add_complex_by_real(angle, 1);
    Complex res1 = complex_pow_real(root1, 0.5);
    Complex root2 = sub_complex_by_real(angle, 1);
    Complex res2 = complex_pow_real(root2, 0.5);
    Complex prod = mul_complex_numbers(res1, res2);
    Complex log = add_complex_numbers(prod, angle);
    Complex num = complex_log_real(log, M_E);
    return num;
}

Complex acosh_real(double angle) {
    Complex num = new_Complex_algeb(acosh(angle), 0);
    return num;
}

Complex acosh_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = acosh_complex(temp);
    return num;
}

Complex atanh_complex(Complex angle) {
    Complex nom = add_complex_by_real(angle, 1);
    Complex denom = sub_real_by_complex(angle, 1);
    Complex log = div_complex_numbers(nom, denom);
    Complex ans = complex_log_real(log, M_E);
    Complex num = div_complex_by_real(ans, 2);
    return num;
}

Complex atanh_real(double angle) {
    Complex num = new_Complex_algeb(atanh(angle), 0);
    return num;
}

Complex atanh_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = atanh_complex(temp);
    return num;
}

// multiplicative inverse
Complex sech_complex(Complex angle) {
    Complex temp = cosh_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex sech_real(double angle) {
    Complex temp = cosh_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex sech_imag(double angle) {
    Complex temp = cosh_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex cosech_complex(Complex angle) {
    Complex temp = sinh_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex cosech_real(double angle) {
    Complex temp = sinh_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex cosech_imag(double angle) {
    Complex temp = sinh_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex cotanh_complex(Complex angle) {
    Complex temp = tanh_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex cotanh_real(double angle) {
    Complex temp = tanh_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex cotanh_imag(double angle) {
    Complex temp = tanh_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

// inverse of other functions
Complex asech_complex(Complex angle) {
    Complex temp = acosh_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex asech_real(double angle) {
    Complex temp = acosh_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex asech_imag(double angle) {
    Complex temp = acosh_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acosech_complex(Complex angle) {
    Complex temp = asinh_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acosech_real(double angle) {
    Complex temp = asinh_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acosech_imag(double angle) {
    Complex temp = asinh_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acotanh_complex(Complex angle) {
    Complex temp = atanh_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acotanh_real(double angle) {
    Complex temp = atanh_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acotanh_imag(double angle) {
    Complex temp = atanh_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}
