// This file has been created on February-28-2020
// Copyright (c) 2020 Abanoub Sameh

#include "headers/complex_algebra.h"

Complex add_complex_numbers(Complex num1, Complex num2) {
    Complex num = new_Complex_algeb(num1.real + num2.real, num1.imag + num2.imag);
    //delete_Complex(num1);
    //delete_Complex(num2);
    return num;
}

Complex sub_complex_numbers(Complex num1, Complex num2) {
    Complex num = new_Complex_algeb(num1.real - num2.real, num1.imag - num2.imag);
    //delete_Complex(num1);
    //delete_Complex(num2);
    return num;
}

Complex mul_complex_numbers(Complex num1, Complex num2) {
    Complex num = new_Complex_algeb(num1.real * num2.real - num1.imag * num2.imag,
                        num1.imag * num2.real + num2.imag * num1.real);
    //delete_Complex(num1);
    //delete_Complex(num2);
    return num;
}

Complex div_complex_numbers(Complex num1, Complex num2) {
    Complex temp = new_Complex_algeb(num2.real, -num2.imag);
    //print_Complex_newLine(num2);
    //print_Complex_newLine(temp);
    Complex temp2 = mul_complex_numbers(num1, temp);
    //print_Complex_newLine(temp2);
    Complex num = div_complex_by_real(temp2,
                        num2.real * num2.real + num2.imag * num2.imag);
    return num;
}

Complex add_complex_by_real(Complex num1, double real) {
    Complex num = new_Complex_algeb(num1.real + real, num1.imag);
    return num;
}

Complex add_complex_by_imag(Complex num1, double imag) {
    Complex num = new_Complex_algeb(num1.real, num1.imag + imag);
    return num;
}

Complex sub_complex_by_real(Complex num1, double real) {
    Complex num = new_Complex_algeb(num1.real - real, num1.imag);
    return num;
}

Complex sub_complex_by_imag(Complex num1, double imag) {
    Complex num = new_Complex_algeb(num1.real, num1.imag - imag);
    return num;
}

Complex sub_real_by_complex(Complex num1, double real) {
    Complex num = new_Complex_algeb(real - num1.real, -num1.imag);
    return num;
}

Complex sub_imag_by_complex(Complex num1, double imag) {
    Complex num = new_Complex_algeb(-num1.real, imag - num1.imag);
    return num;
}

Complex mul_complex_by_real(Complex num1, double real) {
    Complex num = new_Complex_algeb(num1.real * real, num1.imag * real);
    return num;
}

Complex mul_complex_by_imag(Complex num1, double imag) {
    Complex num = new_Complex_algeb(num1.imag * -imag, num1.real * imag);
    return num;
}

Complex div_complex_by_real(Complex num1, double real) {
    Complex num = new_Complex_algeb(num1.real / real, num1.imag / real);
    return num;
}

Complex div_complex_by_imag(Complex num1, double imag) {
    Complex temp = new_Complex_algeb(num1.imag, -num1.real);
    Complex num = div_complex_by_real(temp, imag);
    return num;
}

Complex div_real_by_complex(Complex num1, double real) {
    Complex temp = new_Complex_algeb(num1.real, -num1.imag);
    Complex temp2 = mul_complex_by_real(temp, real);
    double denom = num1.real * num1.real + num1.imag * num1.imag;
    Complex num = new_Complex_algeb(temp2.real / denom, temp2.imag / denom);
    return num;
}

Complex div_imag_by_complex(Complex num1, double imag) {
    Complex inv = new_Complex_algeb(num1.real, -num1.imag);
    Complex nominator = mul_complex_by_imag(inv, imag);
    double denominator = num1.real * num1.real + num1.imag * num1.imag;
    Complex num = div_complex_by_real(nominator, denominator);
    return num;
}
