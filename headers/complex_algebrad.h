// This file has been created on February-28-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _complex_algebrad_h_
#define _complex_algebrad_h_

#include "Complexd.h"

Complex *add_complex_numbersd(Complex*, Complex*);
Complex *sub_complex_numbersd(Complex*, Complex*);
Complex *mul_complex_numbersd(Complex*, Complex*);
Complex *div_complex_numbersd(Complex*, Complex*);

Complex *add_complex_by_reald(Complex*, double);
Complex *add_complex_by_imagd(Complex*, double);

Complex *sub_complex_by_reald(Complex*, double);
Complex *sub_complex_by_imagd(Complex*, double);
Complex *sub_real_by_complexd(Complex*, double);
Complex *sub_imag_by_complexd(Complex*, double);

Complex *mul_complex_by_reald(Complex*, double);
Complex *mul_complex_by_imagd(Complex*, double);

Complex *div_complex_by_reald(Complex*, double);
Complex *div_complex_by_imagd(Complex*, double);
Complex *div_real_by_complexd(Complex*, double);
Complex *div_imag_by_complexd(Complex*, double);

#endif
