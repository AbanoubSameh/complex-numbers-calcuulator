// This file has been created on March-14-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _complex_def_h_
#define _complex_def_h_

typedef struct Complex {
    double real;
    double imag;
} Complex;

#endif
