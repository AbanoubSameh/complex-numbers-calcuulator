// This file has been created on February-28-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _complexd_h_
#define _complexd_h_

#include "Complex_def.h"

Complex *new_Complex_algebd(double, double);
Complex *new_Complex_polard(double, double);
void delete_Complexd(Complex*);

void change_algebriacd(Complex*, double, double);
void change_module_and_angled(Complex*, double, double);

double get_moduled(Complex*);
double get_angled(Complex*);

void print_algebriac_formd(Complex*);
void print_polar_formd(Complex*);
void print_trig_formd(Complex*);

void print_algebriac_form_nld(Complex*);
void print_polar_form_nld(Complex*);
void print_trig_form_nld(Complex*);

#endif
