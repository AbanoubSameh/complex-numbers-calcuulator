// This file has been created on February-29-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _complex_hyperbolicd_h_
#define _complex_hyperbolicd_h_

#include "Complexd.h"

// hyperbolic functions
Complex sinh_complex(Complex);
Complex sinh_real(double);
Complex sinh_imag(double);

Complex cosh_complex(Complex);
Complex cosh_real(double);
Complex cosh_imag(double);

Complex tanh_complex(Complex);
Complex tanh_real(double);
Complex tanh_imag(double);

// inverse hyperbolic functions
Complex asinh_complex(Complex);
Complex asinh_real(double);
Complex asinh_imag(double);

Complex acosh_complex(Complex);
Complex acosh_real(double);
Complex acosh_imag(double);

Complex atanh_complex(Complex);
Complex atanh_real(double);
Complex atanh_imag(double);

// multiplicative inverse
Complex sech_complex(Complex);
Complex sech_real(double);
Complex sech_imag(double);

Complex cosech_complex(Complex);
Complex cosech_real(double);
Complex cosech_imag(double);

Complex cotanh_complex(Complex);
Complex cotanh_real(double);
Complex cotanh_imag(double);

// inverse of other functions
Complex asech_complex(Complex);
Complex asech_real(double);
Complex asech_imag(double);

Complex acosech_complex(Complex);
Complex acosech_real(double);
Complex acosech_imag(double);

Complex acotanh_complex(Complex);
Complex acotanh_real(double);
Complex acotanh_imag(double);
#endif
