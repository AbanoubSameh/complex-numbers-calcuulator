// This file has been created on February-28-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _complex_algebra_h_
#define _complex_algebra_h_

#include "Complex.h"

Complex add_complex_numbers(Complex, Complex);
Complex sub_complex_numbers(Complex, Complex);
Complex mul_complex_numbers(Complex, Complex);
Complex div_complex_numbers(Complex, Complex);

Complex add_complex_by_real(Complex, double);
Complex add_complex_by_imag(Complex, double);

Complex sub_complex_by_real(Complex, double);
Complex sub_complex_by_imag(Complex, double);
Complex sub_real_by_complex(Complex, double);
Complex sub_imag_by_complex(Complex, double);

Complex mul_complex_by_real(Complex, double);
Complex mul_complex_by_imag(Complex, double);

Complex div_complex_by_real(Complex, double);
Complex div_complex_by_imag(Complex, double);
Complex div_real_by_complex(Complex, double);
Complex div_imag_by_complex(Complex, double);

#endif
