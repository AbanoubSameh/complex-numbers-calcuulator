// This file has been created on February-29-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _complex_hyperbolicd_h_
#define _complex_hyperbolicd_h_

#include "Complexd.h"

// hyperbolic functions
Complex *sinh_complexd(Complex*);
Complex *sinh_reald(double);
Complex *sinh_imagd(double);

Complex *cosh_complexd(Complex*);
Complex *cosh_reald(double);
Complex *cosh_imagd(double);

Complex *tanh_complexd(Complex*);
Complex *tanh_reald(double);
Complex *tanh_imagd(double);

// inverse hyperbolic functions
Complex *asinh_complexd(Complex*);
Complex *asinh_reald(double);
Complex *asinh_imagd(double);

Complex *acosh_complexd(Complex*);
Complex *acosh_reald(double);
Complex *acosh_imagd(double);

Complex *atanh_complexd(Complex*);
Complex *atanh_reald(double);
Complex *atanh_imagd(double);

// multiplicative inverse
Complex *sech_complexd(Complex*);
Complex *sech_reald(double);
Complex *sech_imagd(double);

Complex *cosech_complexd(Complex*);
Complex *cosech_reald(double);
Complex *cosech_imagd(double);

Complex *cotanh_complexd(Complex*);
Complex *cotanh_reald(double);
Complex *cotanh_imagd(double);

// inverse of other functions
Complex *asech_complexd(Complex*);
Complex *asech_reald(double);
Complex *asech_imagd(double);

Complex *acosech_complexd(Complex*);
Complex *acosech_reald(double);
Complex *acosech_imagd(double);

Complex *acotanh_complexd(Complex*);
Complex *acotanh_reald(double);
Complex *acotanh_imagd(double);
#endif
