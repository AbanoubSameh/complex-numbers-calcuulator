// This file has been created on February-28-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _complex_power_h_
#define _complex_power_h_

#include "Complexd.h"

Complex complex_pow_complex(Complex, Complex);

Complex complex_pow_real(Complex, double);
Complex complex_pow_imag(Complex, double);

Complex real_pow_complex(Complex, double);
Complex imag_pow_complex(Complex, double);

Complex complex_log_complex(Complex, Complex);

Complex complex_log_real(Complex, double);
Complex complex_log_imag(Complex, double);

Complex real_log_complex(Complex, double);
Complex imag_log_complex(Complex, double);

Complex ln_complex(Complex);
Complex ln_real(double);
Complex ln_imag(double);

Complex exp_complex(Complex);
Complex exp_real(double);
Complex exp_imag(double);

#endif
