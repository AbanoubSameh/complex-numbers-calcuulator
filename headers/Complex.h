// This file has been created on March-14-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _complex_h_
#define _complex_h_

#include "Complex_def.h"

Complex new_Complex_algeb(double, double);
Complex new_Complex_polar(double, double);

double get_module(Complex);
double get_angle(Complex);

void print_algebriac_form(Complex);
void print_polar_form(Complex);
void print_trig_form(Complex);

void print_algebriac_form_nl(Complex);
void print_polar_form_nl(Complex);
void print_trig_form_nl(Complex);

#endif
