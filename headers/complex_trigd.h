// This file has been created on February-29-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _complex_trigd_h_
#define _complex_trigd_h_

#include "Complexd.h"
#include "complex_powerd.h"

// main functions
Complex *sin_complexd(Complex*);
Complex *sin_reald(double);
Complex *sin_imagd(double);

Complex *cos_complexd(Complex*);
Complex *cos_reald(double);
Complex *cos_imagd(double);

Complex *tan_complexd(Complex*);
Complex *tan_reald(double);
Complex *tan_imagd(double);

// inverse functions
Complex *asin_complexd(Complex*);
Complex *asin_reald(double);
Complex *asin_imagd(double);

Complex *acos_complexd(Complex*);
Complex *acos_reald(double);
Complex *acos_imagd(double);

Complex *atan_complexd(Complex*);
Complex *atan_reald(double);
Complex *atan_imagd(double);

// multiplicative inverse of main functions
Complex *sec_complexd(Complex*);
Complex *sec_reald(double);
Complex *sec_imagd(double);

Complex *cosec_complexd(Complex*);
Complex *cosec_reald(double);
Complex *cosec_imagd(double);

Complex *cotan_complexd(Complex*);
Complex *cotan_reald(double);
Complex *cotan_imagd(double);

// inverse of the reset
Complex *asec_complexd(Complex*);
Complex *asec_reald(double);
Complex *asec_imagd(double);

Complex *acosec_complexd(Complex*);
Complex *acosec_reald(double);
Complex *acosec_imagd(double);

Complex *acotan_complexd(Complex*);
Complex *acotan_reald(double);
Complex *acotan_imagd(double);

#endif
