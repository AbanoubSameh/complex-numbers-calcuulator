// This file has been created on February-28-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _complex_powerd_h_
#define _complex_powerd_h_

#include "Complexd.h"

Complex *complex_pow_complexd(Complex*, Complex*);

Complex *complex_pow_reald(Complex*, double);
Complex *complex_pow_imagd(Complex*, double);

Complex *real_pow_complexd(Complex*, double);
Complex *imag_pow_complexd(Complex*, double);

Complex *complex_log_complexd(Complex*, Complex*);

Complex *complex_log_reald(Complex*, double);
Complex *complex_log_imagd(Complex*, double);

Complex *real_log_complexd(Complex*, double);
Complex *imag_log_complexd(Complex*, double);

Complex *ln_complexd(Complex*);
Complex *ln_reald(double);
Complex *ln_imagd(double);

Complex *exp_complexd(Complex*);
Complex *exp_reald(double);
Complex *exp_imagd(double);

#endif
