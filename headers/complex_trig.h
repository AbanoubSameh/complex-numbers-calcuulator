// This file has been created on February-29-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _complex_trigd_h_
#define _complex_trigd_h_

#include "Complexd.h"
#include "complex_powerd.h"

// main functions
Complex sin_complex(Complex);
Complex sin_real(double);
Complex sin_imag(double);

Complex cos_complex(Complex);
Complex cos_real(double);
Complex cos_imag(double);

Complex tan_complex(Complex);
Complex tan_real(double);
Complex tan_imag(double);

// inverse functions
Complex asin_complex(Complex);
Complex asin_real(double);
Complex asin_imag(double);

Complex acos_complex(Complex);
Complex acos_real(double);
Complex acos_imag(double);

Complex atan_complex(Complex);
Complex atan_real(double);
Complex atan_imag(double);

// multiplicative inverse of main functions
Complex sec_complex(Complex);
Complex sec_real(double);
Complex sec_imag(double);

Complex cosec_complex(Complex);
Complex cosec_real(double);
Complex cosec_imag(double);

Complex cotan_complex(Complex);
Complex cotan_real(double);
Complex cotan_imag(double);

// inverse of the reset
Complex asec_complex(Complex);
Complex asec_real(double);
Complex asec_imag(double);

Complex acosec_complex(Complex);
Complex acosec_real(double);
Complex acosec_imag(double);

Complex acotan_complex(Complex);
Complex acotan_real(double);
Complex acotan_imag(double);

#endif
