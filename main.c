#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "headers/Complex.h"
#include "headers/complex_algebra.h"
#include "headers/complex_power.h"
#include "headers/complex_trig.h"
#include "headers/complex_hyperbolic.h"

int main() {
    /*
    Complex num = new_Complex_algeb(12, 34);
    Complex new = new_Complex_algeb(12, 10);
    print_algebriac_form_nl(num);

    puts("\npower:");
    print_algebriac_form_nl(complex_pow_real(num, 2));
    print_algebriac_form_nl(complex_pow_imag(num, 2));
    print_algebriac_form_nl(complex_pow_complex(num, new));

    puts("\nlog:");
    print_algebriac_form_nl(complex_log_real(num, 2));
    print_algebriac_form_nl(complex_log_imag(num, 2));
    print_algebriac_form_nl(complex_log_complex(num, num));

    puts("\ntrigonometric:");
    print_algebriac_form_nl(sin_complex(num));
    print_algebriac_form_nl(cos_complex(num));
    print_algebriac_form_nl(tan_complex(num));

    print_algebriac_form_nl(asin_complex(num));
    print_algebriac_form_nl(acos_complex(num));
    print_algebriac_form_nl(atan_complex(num));


    print_algebriac_form_nl(sec_complex(num));
    print_algebriac_form_nl(cosec_complex(num));
    print_algebriac_form_nl(cotan_complex(num));

    print_algebriac_form_nl(asec_complex(num));
    print_algebriac_form_nl(acosec_complex(num));
    print_algebriac_form_nl(acotan_complex(num));

    puts("\nhyperbolic:");
    print_algebriac_form_nl(sinh_complex(num));
    print_algebriac_form_nl(cosh_complex(num));
    print_algebriac_form_nl(tanh_complex(num));

    print_algebriac_form_nl(asinh_complex(num));
    print_algebriac_form_nl(acosh_complex(num));
    print_algebriac_form_nl(atanh_complex(num));

    print_algebriac_form_nl(sech_complex(num));
    print_algebriac_form_nl(cosech_complex(num));
    print_algebriac_form_nl(cotanh_complex(num));

    print_algebriac_form_nl(asech_complex(num));
    print_algebriac_form_nl(acosech_complex(num));
    print_algebriac_form_nl(acotanh_complex(num));
    */
    print_algebriac_form_nl(cos_imag(1));
    Complex i = new_Complex_algeb(0, 1);
    print_algebriac_form_nl(complex_pow_complex(i, i));
}
