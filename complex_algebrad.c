// This file has been created on February-28-2020
// Copyright (c) 2020 Abanoub Sameh

#include "headers/complex_algebrad.h"

Complex *add_complex_numbersd(Complex *num1, Complex *num2) {
    Complex *num = new_Complex_algebd(num1->real + num2->real, num1->imag + num2->imag);
    //delete_Complex(num1);
    //delete_Complex(num2);
    return num;
}

Complex *sub_complex_numbersd(Complex *num1, Complex *num2) {
    Complex *num = new_Complex_algebd(num1->real - num2->real, num1->imag - num2->imag);
    //delete_Complex(num1);
    //delete_Complex(num2);
    return num;
}

Complex *mul_complex_numbersd(Complex *num1, Complex *num2) {
    Complex *num = new_Complex_algebd(num1->real * num2->real - num1->imag * num2->imag,
                        num1->imag * num2->real + num2->imag * num1->real);
    //delete_Complex(num1);
    //delete_Complex(num2);
    return num;
}

Complex *div_complex_numbersd(Complex *num1, Complex *num2) {
    Complex *temp = new_Complex_algebd(num2->real, -num2->imag);
    //print_Complex_newLine(num2);
    //print_Complex_newLine(temp);
    Complex *temp2 = mul_complex_numbersd(num1, temp);
    //print_Complex_newLine(temp2);
    Complex *num = div_complex_by_reald(temp2,
                        num2->real * num2->real + num2->imag * num2->imag);
    delete_Complexd(temp);
    delete_Complexd(temp2);
    //delete_Complex(num1);
    //delete_Complex(num2);
    return num;
}

Complex *add_complex_by_reald(Complex *num1, double real) {
    Complex *num = new_Complex_algebd(num1->real + real, num1->imag);
    //delete_Complex(num1);
    return num;
}

Complex *add_complex_by_imagd(Complex *num1, double imag) {
    Complex *num = new_Complex_algebd(num1->real, num1->imag + imag);
    //delete_Complex(num1);
    return num;
}

Complex *sub_complex_by_reald(Complex *num1, double real) {
    Complex *num = new_Complex_algebd(num1->real - real, num1->imag);
    //delete_Complex(num1);
    return num;
}

Complex *sub_complex_by_imagd(Complex *num1, double imag) {
    Complex *num = new_Complex_algebd(num1->real, num1->imag - imag);
    //delete_Complex(num1);
    return num;
}

Complex *sub_real_by_complexd(Complex *num1, double real) {
    Complex *num = new_Complex_algebd(real - num1->real, -num1->imag);
    //delete_Complex(num1);
    return num;
}

Complex *sub_imag_by_complexd(Complex *num1, double imag) {
    Complex *num = new_Complex_algebd(-num1->real, imag - num1->imag);
    //delete_Complex(num1);
    return num;
}

Complex *mul_complex_by_reald(Complex *num1, double real) {
    Complex *num = new_Complex_algebd(num1->real * real, num1->imag * real);
    //delete_Complex(num1);
    return num;
}

Complex *mul_complex_by_imagd(Complex *num1, double imag) {
    Complex *num = new_Complex_algebd(num1->imag * -imag, num1->real * imag);
    return num;
}

Complex *div_complex_by_reald(Complex *num1, double real) {
    Complex *num = new_Complex_algebd(num1->real / real, num1->imag / real);
    //delete_Complex(num1);
    return num;
}

Complex *div_complex_by_imagd(Complex *num1, double imag) {
    Complex *temp = new_Complex_algebd(num1->imag, -num1->real);
    Complex *num = div_complex_by_reald(temp, imag);
    delete_Complexd(temp);
    return num;
}

Complex *div_real_by_complexd(Complex *num1, double real) {
    Complex *temp = new_Complex_algebd(num1->real, -num1->imag);
    Complex *temp2 = mul_complex_by_reald(temp, real);
    double denom = num1->real * num1->real + num1->imag * num1->imag;
    Complex *num = new_Complex_algebd(temp2->real / denom, temp2->imag / denom);
    delete_Complexd(temp);
    delete_Complexd(temp2);
    //delete_Complex(num1);
    return num;
}

Complex *div_imag_by_complexd(Complex *num1, double imag) {
    Complex *inv = new_Complex_algebd(num1->real, -num1->imag);
    Complex *nominator = mul_complex_by_imagd(inv, imag);
    double denominator = num1->real * num1->real + num1->imag * num1->imag;
    Complex *num = div_complex_by_reald(nominator, denominator);
    delete_Complexd(inv);
    delete_Complexd(nominator);
    return num;
}
