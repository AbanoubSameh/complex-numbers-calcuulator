// This file has been created on February-28-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "headers/Complex.h"

Complex new_Complex_algeb(double real, double imag) {
    Complex num;
    num.real = real;
    num.imag = imag;
    return num;
}

Complex new_Complex_polar(double module, double angle) {
    Complex num;
    double real = module * cos(angle);
    double imag = module * sin(angle);
    if (real < 0.000000001 && real > 0) num.real = 0;
    else if (real > -0.000000001 && real < 0) num.real = 0;
    else num.real = real;

    if (imag < 0.000000001 && imag > 0) num.imag = 0;
    else if (imag > -0.000000001 && imag < 0) num.imag = 0;
    else num.imag = imag;
    return num;
}

double get_module(Complex num) {
    return sqrt(num.real * num.real + num.imag * num.imag);
}

double get_angle(Complex num) {
    //return atan(num.imag / num.real)  180 / M_PI;
    double angle;

    if ((num.real >= 0 && num.imag >= 0) || (num.real >= 0 && num.imag < 0))
        angle = atan(num.imag / num.real);
    else if (num.real < 0 && num.imag >= 0)
        angle = atan(num.imag / num.real) + M_PI;
    else if (num.real < 0 && num.imag < 0)
        angle = atan(num.imag / num.real) - M_PI;

    if (angle < 0.000000001 && angle > 0) return 0;
    else if (angle > -0.000000001 && angle < 0) return M_PI;
    else return angle;
}

void print_algebriac_form(Complex num) {
    if (num.imag == 0) printf("%.9g", num.real);
    else if (num.imag == 1 && num.real == 0) printf("i");
    else if (num.imag == -1 && num.real == 0) printf("-i");
    else if (num.imag == 1 && num.real == 0) printf("%.9g+i", num.real);
    else if (num.imag == -1 && num.real == 0) printf("%.9g-i", num.real);
    else if (num.imag > 0) printf("%.9g+%.9gi", num.real, num.imag);
    else if (num.imag < 0) printf("%.9g%.9gi", num.real, num.imag);
}

void print_polar_form(Complex num) {
    double module = get_module(num);
    double angle = get_angle(num);
    if (module == 0) printf("0");
    else if (angle == 0) printf("%.9g", module);
    else if (angle == 1) printf("%.9ge^i", module);
    else if (angle == -1) printf("%.9ge^-i", module);
    else printf("%.9ge^%.9gi", module, angle);
}

void print_trig_form(Complex num) {
    //puts("printing trig");
    double angle = get_angle(num);
    printf("%.9g(cos(%.9g)+sin(%.9g)i)", get_module(num), angle, angle);
}

void print_algebriac_form_nl(Complex num) {
    print_algebriac_form(num);
    puts("");
}

void print_polar_form_nl(Complex num) {
    print_polar_form(num);
    puts("");
}

void print_trig_form_nl(Complex num) {
    print_trig_form(num);
    puts("");
}
