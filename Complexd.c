// This file has been created on February-28-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "headers/Complexd.h"

Complex *new_Complex_algebd(double real, double imag) {
    Complex *num = malloc(sizeof(Complex));
    change_algebriacd(num, real, imag);
    //puts("new_Complex_algeb created");
    return num;
}

Complex *new_Complex_polard(double module, double angle) {
    Complex *num = malloc(sizeof(Complex));
    change_module_and_angled(num, module, angle);
    //puts("new_Complex_polar created");
    return num;
}

void delete_Complexd(Complex *num) {
    free(num);
}

void change_algebriacd(Complex *num, double real, double imag) {
    num->real = real;
    num->imag = imag;
}

void change_module_and_angled(Complex *num, double module, double angle) {
    //num->real = module * cos(angle * M_PI / 180);
    //num->imag = module * sin(angle * M_PI / 180);
    double real = module * cos(angle);
    double imag = module * sin(angle);
    if (real < 0.000000001 && real > 0) num->real = 0;
    else if (real > -0.000000001 && real < 0) num->real = 0;
    else num->real = real;

    if (imag < 0.000000001 && imag > 0) num->imag = 0;
    else if (imag > -0.000000001 && imag < 0) num->imag = 0;
    else num->imag = imag;
}

double get_moduled(Complex *num) {
    return sqrt(num->real * num->real + num->imag * num->imag);
}

double get_angled(Complex *num) {
    //return atan(num->imag / num->real) * 180 / M_PI;
    double angle;

    if ((num->real >= 0 && num->imag >= 0) || (num->real >= 0 && num->imag < 0))
        angle = atan(num->imag / num->real);
    else if (num->real < 0 && num->imag >= 0)
        angle = atan(num->imag / num->real) + M_PI;
    else if (num->real < 0 && num->imag < 0)
        angle = atan(num->imag / num->real) - M_PI;

    if (angle < 0.000000001 && angle > 0) return 0;
    else if (angle > -0.000000001 && angle < 0) return M_PI;
    else return angle;
}

void print_algebriac_formd(Complex *num) {
    if (num->imag == 0) printf("%.9g", num->real);
    else if (num->imag == 1 && num->real == 0) printf("i");
    else if (num->imag == -1 && num->real == 0) printf("-i");
    else if (num->imag == 1 && num->real == 0) printf("%.9g+i", num->real);
    else if (num->imag == -1 && num->real == 0) printf("%.9g-i", num->real);
    else if (num->imag > 0) printf("%.9g+%.9gi", num->real, num->imag);
    else if (num->imag < 0) printf("%.9g%.9gi", num->real, num->imag);
}

void print_polar_formd(Complex *num) {
    double module = get_moduled(num);
    double angle = get_angled(num);
    if (module == 0) printf("0");
    else if (angle == 0) printf("%.9g", module);
    else if (angle == 1) printf("%.9ge^i", module);
    else if (angle == -1) printf("%.9ge^-i", module);
    else printf("%.9ge^%.9gi", module, angle);
}

void print_trig_formd(Complex *num) {
    //puts("printing trig");
    double angle = get_angled(num);
    printf("%.9g(cos(%.9g)+sin(%.9g)i)", get_moduled(num), angle, angle);
}

void print_algebriac_form_nld(Complex *num) {
    print_algebriac_formd(num);
    puts("");
}

void print_polar_form_nld(Complex *num) {
    print_polar_formd(num);
    puts("");
}

void print_trig_form_nld(Complex *num) {
    print_trig_formd(num);
    puts("");
}
