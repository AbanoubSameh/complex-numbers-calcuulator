// This file has been created on February-28-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <math.h>

#include "headers/complex_power.h"
#include "headers/complex_algebra.h"

Complex complex_pow_complex(Complex num1, Complex num2) {
    Complex part1 = complex_pow_real(num1, num2.real);
    Complex ln = complex_log_real(num1, M_E);
    Complex real_pow = mul_complex_by_real(ln, num2.imag);
    Complex i = new_Complex_algeb(0, 1);
    Complex pow = mul_complex_numbers(real_pow, i);
    Complex part2 = real_pow_complex(pow, M_E);
    Complex num = mul_complex_numbers(part1, part2);
    return num;
}

Complex complex_pow_real(Complex num1, double real) {
    double module = get_module(num1);
    //printf("%f\n", module);
    //printf("%f\n", get_angle(num1));
    //printf("%f\n", pow(module, real));
    Complex num = new_Complex_polar(pow(module, real), get_angle(num1) * real);
    return num;
}

Complex complex_pow_imag(Complex num1, double imag) {
    //double module = get_module(num1);
    Complex temp = new_Complex_algeb(0, imag);
    Complex num = complex_pow_complex(num1, temp);
    //Complex num = new_Complex_polar(pow(module, real), get_angle(num1)  real);
    return num;
}

Complex real_pow_complex(Complex num1, double real) {
    double module = pow(real, num1.real);
    double angle = num1.imag * log(real);
    Complex num = new_Complex_polar(module, angle);
    return num;
}

Complex imag_pow_complex(Complex num1, double imag) {
    Complex temp = new_Complex_algeb(0, imag);
    Complex num = complex_pow_complex(temp, num1);
    return num;
}

Complex complex_log_complex(Complex num1, Complex base) {
    Complex nom = complex_log_real(num1, M_E);
    Complex denom = complex_log_real(base, M_E);
    Complex num = div_complex_numbers(nom, denom);
    return num;
}

Complex complex_log_real(Complex num1, double real) {
    double base = log(real);
    //printf("%f\n", get_angle(num1));
    Complex num = new_Complex_algeb(log(get_module(num1)) / base, get_angle(num1) / base);
    return num;
}

Complex complex_log_imag(Complex num1, double imag) {
    Complex temp = new_Complex_algeb(0, imag);
    Complex num = complex_log_complex(num1, temp);
    return num;
}

Complex real_log_complex(Complex num1, double real) {
    Complex num = div_real_by_complex(complex_log_real(num1, M_E), log(real));
    return num;
}

Complex imag_log_complex(Complex num1, double imag) {
    Complex temp = new_Complex_algeb(0, imag);
    Complex num = complex_log_complex(temp, num1);
    return num;
}

Complex exp_complex(Complex num1) {
    return real_pow_complex(num1, M_E);
}

Complex exp_real(double num1) {
    Complex num = new_Complex_algeb(num1, 0);
    return real_pow_complex(num, M_E);
}

Complex exp_imag(double num1) {
    Complex num = new_Complex_algeb(0, num1);
    return real_pow_complex(num, M_E);
}

Complex ln_complex(Complex num1) {
    return complex_log_real(num1, M_E);
}

Complex ln_real(double num1) {
    Complex num = new_Complex_algeb(num1, 0);
    return complex_log_real(num, M_E);
}

Complex ln_imag(double num1) {
    Complex num = new_Complex_algeb(0, num1);
    return complex_log_real(num, M_E);
}
