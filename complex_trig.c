// This file has been created on February-29-2020
// Copyright (c) 2020 Abanoub Sameh

#include <math.h>

#include "headers/Complex.h"
#include "headers/complex_algebra.h"
#include "headers/complex_power.h"

// main functions
Complex sin_complex(Complex angle) {
    Complex num = new_Complex_algeb(sin(angle.real) * cosh(angle.imag),
                        sinh(angle.imag) * cos(angle.real));
    return num;
}

Complex sin_real(double angle) {
    Complex num = new_Complex_algeb(sin(angle), 0);
    return num;
}

Complex sin_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = sin_complex(temp);
    return num;
}

Complex cos_complex(Complex angle) {
    Complex num = new_Complex_algeb(cos(angle.real) * cosh(angle.imag),
                        -sinh(angle.imag) * sin(angle.real));
    return num;
}

Complex cos_real(double angle) {
    Complex num = new_Complex_algeb(cos(angle), 0);
    return num;
}

Complex cos_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = cos_complex(temp);
    return num;
}

Complex tan_complex(Complex angle) {
    Complex nom = sin_complex(angle);
    Complex denom = cos_complex(angle);
    Complex num = div_complex_numbers(nom, denom);
    return num;
}

Complex tan_real(double angle) {
    Complex num = new_Complex_algeb(tan(angle), 0);
    return num;
}

Complex tan_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = tan_complex(temp);
    return num;
}

// inverse of main functions
Complex asin_complex(Complex angle) {
    Complex part1 = mul_complex_by_imag(angle, 1);
    Complex sub = complex_pow_real(angle, 2);
    Complex root = sub_real_by_complex(sub, 1);
    Complex part2 = complex_pow_real(root, 0.5);
    Complex add = add_complex_numbers(part1, part2);
    Complex ln = complex_log_real(add, M_E);
    Complex num = mul_complex_by_imag(ln, -1);
    return num;
}

Complex asin_real(double angle) {
    Complex num = new_Complex_algeb(asin(angle), 0);
    return num;
}

Complex asin_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = asin_complex(temp);
    return num;
}

Complex acos_complex(Complex angle) {
    Complex asin = asin_complex(angle);
    Complex num = sub_real_by_complex(asin, M_PI / 2);
    return num;
}

Complex acos_real(double angle) {
    Complex num = new_Complex_algeb(acos(angle), 0);
    return num;
}

Complex acos_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = acos_complex(temp);
    return num;
}

Complex atan_complex(Complex angle) {
    Complex i = new_Complex_algeb(0, 1);
    Complex part1 = div_complex_by_real(i, 2);
    Complex nomin = add_complex_numbers(i, angle);
    Complex denomin = sub_complex_numbers(i, angle);
    Complex ln = div_complex_numbers(nomin, denomin);
    Complex part2 = complex_log_real(ln, M_E);
    Complex num = mul_complex_numbers(part1, part2);
    return num;
}

Complex atan_real(double angle) {
    Complex num = new_Complex_algeb(atan(angle), 0);
    return num;
}

Complex atan_imag(double angle) {
    Complex temp = new_Complex_algeb(0, angle);
    Complex num = atan_complex(temp);
    return num;
}

// multiplicative inverse of main functions
Complex sec_complex(Complex angle) {
    Complex temp = cos_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}
Complex sec_real(double angle) {
    Complex temp = cos_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}
Complex sec_imag(double angle) {
    Complex temp = cos_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex cosec_complex(Complex angle) {
    Complex  temp = sin_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex cosec_real(double angle) {
    Complex temp = sin_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex cosec_imag(double angle) {
    Complex temp = sin_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex cotan_complex(Complex angle) {
    Complex temp = tan_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex cotan_real(double angle) {
    Complex temp = tan_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}
Complex cotan_imag(double angle) {
    Complex temp = tan_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

// inverse of the reset
Complex asec_complex(Complex angle) {
    Complex temp = acos_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex asec_real(double angle) {
    Complex temp = acos_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex asec_imag(double angle) {
    Complex temp = acos_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acosec_complex(Complex  angle) {
    Complex temp = asin_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acosec_real(double angle) {
    Complex temp = asin_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acosec_imag(double angle) {
    Complex temp = asin_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acotan_complex(Complex angle) {
    Complex temp = atan_complex(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acotan_real(double angle) {
    Complex temp = atan_real(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}

Complex acotan_imag(double angle) {
    Complex temp = atan_imag(angle);
    Complex num = div_real_by_complex(temp, 1);
    return num;
}
