
SRC = .
OBJ = obj

CFLAGS = -Wall -Wextra -g
LDFLAGS = -lm

FILES = $(OBJ)/main.o $(OBJ)/Complex.o $(OBJ)/complex_algebra.o $(OBJ)/complex_power.o $(OBJ)/complex_trig.o $(OBJ)/complex_hyperbolic.o

complex_calc: $(FILES)
	$(CC) $(CFLAGS) $(LDFLAGS) -o complex_calc $(FILES)

$(OBJ)/%.o: $(SRC)/%.c
	$(CC) $(CFLAGS) -o $@ -c $(SRC)/$<

clean:
	rm -f $(OBJ)/* complex_calc complex_calc.exe

rebuild: clean complex_calc
