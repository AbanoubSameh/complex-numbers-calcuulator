// This file has been created on February-29-2020
// Copyright (c) 2020 Abanoub Sameh

#include <math.h>

#include "headers/Complexd.h"
#include "headers/complex_algebrad.h"
#include "headers/complex_powerd.h"

// main functions
Complex *sin_complexd(Complex *angle) {
    Complex *num = new_Complex_algebd(sin(angle->real) * cosh(angle->imag),
                        sinh(angle->imag) * cos(angle->real));
    return num;
}

Complex *sin_reald(double angle) {
    Complex *num = new_Complex_algebd(sin(angle), 0);
    return num;
}

Complex *sin_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = sin_complexd(temp);
    delete_Complexd(temp);
    return num;
}

Complex *cos_complexd(Complex *angle) {
    Complex *num = new_Complex_algebd(cos(angle->real) * cosh(angle->imag),
                        -sinh(angle->imag) * sin(angle->real));
    return num;
}

Complex *cos_reald(double angle) {
    Complex *num = new_Complex_algebd(cos(angle), 0);
    return num;
}

Complex *cos_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = cos_complexd(temp);
    delete_Complexd(temp);
    return num;
}

Complex *tan_complexd(Complex *angle) {
    Complex *nom = sin_complexd(angle);
    Complex *denom = cos_complexd(angle);
    Complex *num = div_complex_numbersd(nom, denom);
    delete_Complexd(nom);
    delete_Complexd(denom);
    return num;
}

Complex *tan_reald(double angle) {
    Complex *num = new_Complex_algebd(tan(angle), 0);
    return num;
}

Complex *tan_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = tan_complexd(temp);
    delete_Complexd(temp);
    return num;
}

// inverse of main functions
Complex *asin_complexd(Complex *angle) {
    Complex *part1 = mul_complex_by_imagd(angle, 1);
    Complex *sub = complex_pow_reald(angle, 2);
    Complex *root = sub_real_by_complexd(sub, 1);
    Complex *part2 = complex_pow_reald(root, 0.5);
    Complex *add = add_complex_numbersd(part1, part2);
    Complex *ln = complex_log_reald(add, M_E);
    Complex *num = mul_complex_by_imagd(ln, -1);

    delete_Complexd(part1);
    delete_Complexd(sub);
    delete_Complexd(root);
    delete_Complexd(part2);
    delete_Complexd(add);
    delete_Complexd(ln);
    delete_Complexd(num);
    return num;
}

Complex *asin_reald(double angle) {
    Complex *num = new_Complex_algebd(asin(angle), 0);
    return num;
}

Complex *asin_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = asin_complexd(temp);
    delete_Complexd(temp);
    return num;
}

Complex *acos_complexd(Complex *angle) {
    Complex *asin = asin_complexd(angle);
    Complex *num = sub_real_by_complexd(asin, M_PI / 2);
    delete_Complexd(asin);
    return num;
}

Complex *acos_reald(double angle) {
    Complex *num = new_Complex_algebd(acos(angle), 0);
    return num;
}

Complex *acos_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = acos_complexd(temp);
    delete_Complexd(temp);
    return num;
}

Complex *atan_complexd(Complex *angle) {
    Complex *i = new_Complex_algebd(0, 1);
    Complex *part1 = div_complex_by_reald(i, 2);
    Complex *nomin = add_complex_numbersd(i, angle);
    Complex *denomin = sub_complex_numbersd(i, angle);
    Complex *ln = div_complex_numbersd(nomin, denomin);
    Complex *part2 = complex_log_reald(ln, M_E);
    Complex *num = mul_complex_numbersd(part1, part2);

    delete_Complexd(i);
    delete_Complexd(part1);
    delete_Complexd(nomin);
    delete_Complexd(denomin);
    delete_Complexd(ln);
    delete_Complexd(part2);
    return num;
}

Complex *atan_reald(double angle) {
    Complex *num = new_Complex_algebd(atan(angle), 0);
    return num;
}

Complex *atan_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = atan_complexd(temp);
    delete_Complexd(temp);
    return num;
}

// multiplicative inverse of main functions
Complex *sec_complexd(Complex *angle) {
    Complex *temp = cos_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}
Complex *sec_reald(double angle) {
    Complex *temp = cos_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}
Complex *sec_imagd(double angle) {
    Complex *temp = cos_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *cosec_complexd(Complex *angle) {
    Complex  *temp = sin_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *cosec_reald(double angle) {
    Complex *temp = sin_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *cosec_imagd(double angle) {
    Complex *temp = sin_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *cotan_complexd(Complex *angle) {
    Complex *temp = tan_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *cotan_reald(double angle) {
    Complex *temp = tan_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}
Complex *cotan_imagd(double angle) {
    Complex *temp = tan_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

// inverse of the reset
Complex *asec_complexd(Complex *angle) {
    Complex *temp = acos_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *asec_reald(double angle) {
    Complex *temp = acos_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *asec_imagd(double angle) {
    Complex *temp = acos_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acosec_complexd(Complex * angle) {
    Complex *temp = asin_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acosec_reald(double angle) {
    Complex *temp = asin_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acosec_imagd(double angle) {
    Complex *temp = asin_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acotan_complexd(Complex *angle) {
    Complex *temp = atan_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acotan_reald(double angle) {
    Complex *temp = atan_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acotan_imagd(double angle) {
    Complex *temp = atan_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}
