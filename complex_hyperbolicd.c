// This file has been created on March-02-2020
// Copyright (c) 2020 Abanoub Sameh

#include <math.h>

#include "headers/Complexd.h"
#include "headers/complex_algebrad.h"
#include "headers/complex_powerd.h"
#include "headers/complex_hyperbolicd.h"

// hyperbolic functions
Complex *sinh_complexd(Complex *angle) {
    Complex *num = new_Complex_algebd(sinh(angle->real) * cos(angle->imag),
                        sin(angle->imag) * cosh(angle->real));
    return num;
}

Complex *sinh_reald(double angle) {
    Complex *num = new_Complex_algebd(sinh(angle), 0);
    return num;
}

Complex *sinh_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = sinh_complexd(temp);
    delete_Complexd(temp);
    return num;
}

Complex *cosh_complexd(Complex *angle) {
    Complex *num = new_Complex_algebd(cosh(angle->real) * cos(angle->imag),
                        sinh(angle->real) * sin(angle->imag));
    return num;
}

Complex *cosh_reald(double angle) {
    Complex *num = new_Complex_algebd(cosh(angle), 0);
    return num;
}

Complex *cosh_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = cosh_complexd(temp);
    delete_Complexd(temp);
    return num;
}

Complex *tanh_complexd(Complex *angle) {
    Complex *nom = sinh_complexd(angle);
    Complex *denom = cosh_complexd(angle);
    Complex *num = div_complex_numbersd(nom, denom);
    delete_Complexd(nom);
    delete_Complexd(denom);
    return num;
}

Complex *tanh_reald(double angle) {
    Complex *num = new_Complex_algebd(tanh(angle), 0);
    return num;
}

Complex *tanh_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = tanh_complexd(temp);
    delete_Complexd(temp);
    return num;
}

// inverse hyperbolic functions
Complex *asinh_complexd(Complex *angle) {
    Complex *pow = complex_pow_reald(angle, 2);
    Complex *root = add_complex_by_reald(pow, 1);
    Complex *ans = complex_pow_reald(root, 0.5);
    Complex *add = add_complex_numbersd(ans, angle);
    Complex *num = complex_log_reald(add, M_E);

    delete_Complexd(pow);
    delete_Complexd(root);
    delete_Complexd(ans);
    delete_Complexd(add);
    delete_Complexd(num);
    return num;
}

Complex *asinh_reald(double angle) {
    Complex *num = new_Complex_algebd(asinh(angle), 0);
    return num;
}

Complex *asinh_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = asinh_complexd(temp);
    delete_Complexd(temp);
    return num;
}

Complex *acosh_complexd(Complex *angle) {
    Complex *root1 = add_complex_by_reald(angle, 1);
    Complex *res1 = complex_pow_reald(root1, 0.5);
    Complex *root2 = sub_complex_by_reald(angle, 1);
    Complex *res2 = complex_pow_reald(root2, 0.5);
    Complex *prod = mul_complex_numbersd(res1, res2);
    Complex *log = add_complex_numbersd(prod, angle);
    Complex *num = complex_log_reald(log, M_E);

    delete_Complexd(root1);
    delete_Complexd(res1);
    delete_Complexd(root2);
    delete_Complexd(res2);
    delete_Complexd(prod);
    delete_Complexd(log);
    return num;
}

Complex *acosh_reald(double angle) {
    Complex *num = new_Complex_algebd(acosh(angle), 0);
    return num;
}

Complex *acosh_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = acosh_complexd(temp);
    delete_Complexd(temp);
    return num;
}

Complex *atanh_complexd(Complex *angle) {
    Complex *nom = add_complex_by_reald(angle, 1);
    Complex *denom = sub_real_by_complexd(angle, 1);
    Complex *log = div_complex_numbersd(nom, denom);
    Complex *ans = complex_log_reald(log, M_E);
    Complex *num = div_complex_by_reald(ans, 2);

    delete_Complexd(nom);
    delete_Complexd(denom);
    delete_Complexd(log);
    delete_Complexd(ans);
    return num;
}

Complex *atanh_reald(double angle) {
    Complex *num = new_Complex_algebd(atanh(angle), 0);
    return num;
}

Complex *atanh_imagd(double angle) {
    Complex *temp = new_Complex_algebd(0, angle);
    Complex *num = atanh_complexd(temp);
    delete_Complexd(temp);
    return num;
}

// multiplicative inverse
Complex *sech_complexd(Complex* angle) {
    Complex *temp = cosh_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *sech_reald(double angle) {
    Complex *temp = cosh_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *sech_imagd(double angle) {
    Complex *temp = cosh_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *cosech_complexd(Complex *angle) {
    Complex *temp = sinh_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *cosech_reald(double angle) {
    Complex *temp = sinh_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *cosech_imagd(double angle) {
    Complex *temp = sinh_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *cotanh_complexd(Complex *angle) {
    Complex *temp = tanh_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *cotanh_reald(double angle) {
    Complex *temp = tanh_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *cotanh_imagd(double angle) {
    Complex *temp = tanh_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

// inverse of other functions
Complex *asech_complexd(Complex* angle) {
    Complex *temp = acosh_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *asech_reald(double angle) {
    Complex *temp = acosh_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *asech_imagd(double angle) {
    Complex *temp = acosh_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acosech_complexd(Complex *angle) {
    Complex *temp = asinh_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acosech_reald(double angle) {
    Complex *temp = asinh_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acosech_imagd(double angle) {
    Complex *temp = asinh_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acotanh_complexd(Complex *angle) {
    Complex *temp = atanh_complexd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acotanh_reald(double angle) {
    Complex *temp = atanh_reald(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}

Complex *acotanh_imagd(double angle) {
    Complex *temp = atanh_imagd(angle);
    Complex *num = div_real_by_complexd(temp, 1);
    delete_Complexd(temp);
    return num;
}
