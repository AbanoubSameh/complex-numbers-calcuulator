// This file has been created on February-28-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <math.h>

#include "headers/complex_powerd.h"
#include "headers/complex_algebrad.h"

Complex *complex_pow_complex(Complex *num1, Complex *num2) {
    Complex *part1 = complex_pow_reald(num1, num2->real);
    Complex *ln = complex_log_reald(num1, M_E);
    Complex *real_pow = mul_complex_by_reald(ln, num2->imag);
    Complex *i = new_Complex_algebd(0, 1);
    Complex *pow = mul_complex_numbersd(real_pow, i);
    Complex *part2 = real_pow_complexd(pow, M_E);
    Complex *num = mul_complex_numbersd(part1, part2);

    delete_Complexd(part1);
    delete_Complexd(ln);
    delete_Complexd(real_pow);
    delete_Complexd(i);
    delete_Complexd(pow);
    delete_Complexd(part2);
    return num;
}

Complex *complex_pow_reald(Complex *num1, double real) {
    double module = get_moduled(num1);
    //printf("%f\n", module);
    //printf("%f\n", get_angle(num1));
    //printf("%f\n", pow(module, real));
    Complex *num = new_Complex_polard(pow(module, real), get_angled(num1) * real);
    return num;
}

Complex *complex_pow_imag(Complex *num1, double imag) {
    //double module = get_moduled(num1);
    Complex *temp = new_Complex_algebd(0, imag);
    Complex *num = complex_pow_complexd(num1, temp);
    //Complex *num = new_Complex_polar(pow(module, real), get_angle(num1) * real);
    delete_Complexd(temp);
    return num;
}

Complex *real_pow_complexd(Complex *num1, double real) {
    double module = pow(real, num1->real);
    double angle = num1->imag * log(real);
    Complex *num = new_Complex_polard(module, angle);
    return num;
}

Complex *imag_pow_complexd(Complex *num1, double imag) {
    Complex *temp = new_Complex_algebd(0, imag);
    Complex *num = complex_pow_complexd(temp, num1);
    delete_Complexd(temp);
    return num;
}

Complex *complex_log_complexd(Complex *num1, Complex *base) {
    Complex *nom = complex_log_reald(num1, M_E);
    Complex *denom = complex_log_reald(base, M_E);
    Complex *num = div_complex_numbersd(nom, denom);
    delete_Complexd(nom);
    delete_Complexd(denom);
    return num;
}

Complex *complex_log_reald(Complex *num1, double real) {
    double base = log(real);
    //printf("%f\n", get_angle(num1));
    Complex *num = new_Complex_algebd(log(get_moduled(num1)) / base, get_angled(num1) / base);
    return num;
}

Complex *complex_log_imagd(Complex *num1, double imag) {
    Complex *temp = new_Complex_algebd(0, imag);
    Complex *num = complex_log_complexd(num1, temp);
    delete_Complexd(temp);
    return num;
}

Complex *real_log_complexd(Complex *num1, double real) {
    Complex *temp = complex_log_reald(num1, M_E);
    Complex *num = div_real_by_complexd(temp, log(real));
    return num;
}

Complex *imag_log_complexd(Complex *num1, double imag) {
    Complex *temp = new_Complex_algebd(0, imag);
    Complex *num = complex_log_complexd(temp, num1);
    delete_Complexd(temp);
    return num;
}

Complex *exp_complexd(Complex *num1) {
    Complex *num = real_pow_complexd(num1, M_E);
    return num;
}

Complex *exp_reald(double num1) {
    Complex *temp = new_Complex_algebd(num1, 0);
    Complex *num = real_pow_complexd(temp, M_E);
    delete_Complexd(temp);
    return num;
}

Complex *exp_imagd(double num1) {
    Complex *temp = new_Complex_algebd(0, num1);
    Complex *num = real_pow_complexd(temp, M_E);
    delete_Complexd(temp);
    return num;
}

Complex *ln_complexd(Complex *num1) {
    Complex *num = complex_log_reald(num1, M_E);
    return num;
}

Complex *ln_reald(double num1) {
    Complex *temp = new_Complex_algebd(num1, 0);
    Complex *num = complex_log_reald(temp, M_E);
    return num;
}

Complex *ln_imagd(double num1) {
    Complex *temp = new_Complex_algebd(0, num1);
    Complex *num = complex_log_reald(temp, M_E);
    return num;
}
